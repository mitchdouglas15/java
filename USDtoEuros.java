public class DollarstoEuro {
    public static void main(String[] args) {
        int USD = 1;
        int i = 0;
        double Euros = 0.91;
        System.out.print("This program makes a conversion chart from USD to Euros for a few values\n" +
                "USD          Euros\n" +
                "---------    ------\n");
        while (i < 8) {
            if (USD < 10) {
                System.out.println(USD + "            " + Euros);
            }
            else {
                System.out.println(USD + "           " + Euros);
            }
            USD += 1;
            Euros = (USD * 0.91);
            i++;
        }
        System.out.print("Goodbye.");
    }
}
